package me.voidcrafted.bungeeplugins

import me.lucko.luckperms.api.Group
import net.md_5.bungee.api.ChatColor
import net.md_5.bungee.api.chat.BaseComponent
import net.md_5.bungee.chat.ComponentSerializer
import org.json.JSONArray
import org.json.JSONObject

class ChatFormatter(val player_name: String, val player_group: Group, val player_rank: String, val player_discord: String) {
    fun formatServer(server: String): JSONObject {
        var server_short = mapOf(
                "lobby" to "L",
                "survival" to "S"
        )
        var server_description = mapOf(
                "lobby" to "The lobby server",
                "survival" to "The survival server"
        )
        var server_color = mapOf(
                "lobby" to "blue",
                "survival" to "red"
        )
        return JSONObject(mapOf(
                "text" to server_short[server]!!,
                "color" to server_color[server]!!,
                "hoverEvent" to JSONObject(mapOf(
                        "action" to "show_text",
                        "value" to ChatColor.valueOf(server_color[server]!!.toUpperCase()).toString() + server_description[server]!!
                ))
        ))
    }
    fun formatRank(): JSONObject {
        var rank_descriptions = mapOf(
                "tech" to "Server Managers - They write the plugins, manage the servers, and help coordinate the other staff",
                "mafia_don" to "Discord Admins - They chill on the server, chat with players and solve disputes",
                "mcadmin" to "Minecraft Admins - They help with building, moderation, plugins and testing",
                "evtorg" to "Event Org - They run events around the discord and occasionally on the minecraft server",
                "capo" to "Discord Moderators - They chill with the community and have some perms to help people with building etc.",
                "default" to "Players - They don't have a rank :("
        )
        var rank_colors = mapOf(
                "tech" to "gold",
                "mafia_don" to "aqua",
                "mcadmin" to "green",
                "evtorg" to "blue",
                "capo" to "purple",
                "default" to "gray"
        )
        var rank_names = mapOf(
                "tech" to "Mafia Techie",
                "mafia_don" to "Mafia Don",
                "mcadmin" to "Minecraft Admin",
                "evtorg" to "Event Org",
                "capo" to "Mafia Capo",
                "default" to "Member"
        )

        return JSONObject(mapOf(
                "text" to "${rank_names[player_rank]}",
                "color" to rank_colors[player_rank],
                "hoverEvent" to JSONObject(mapOf(
                        "action" to "show_text",
                        "value" to ChatColor.valueOf(rank_colors[player_rank]!!.toUpperCase()).toString() + rank_descriptions[player_rank]
                ))
        ))
    }

    fun formatName(): JSONObject {
        return JSONObject(mapOf(
                "text" to player_name,
                "hoverEvent" to JSONObject(mapOf(
                        "action" to "show_text",
                        "value" to "${ChatColor.BOLD}${ChatColor.GOLD}$player_name\n${ChatColor.RESET}${ChatColor.GOLD}Discord - $player_discord" + if (player_name == "VoidCrafted") "\n${ChatColor.RESET}${ChatColor.RED}Server Owner" else ""
                ))
        ))
    }
    fun format(message:String, server:String): JSONArray {
        var msg = JSONArray()
        msg.put("")
        msg.put("")
        msg.put(this.formatServer(server))
        msg.put(JSONObject(mapOf(
                "text" to " > ",
                "color" to "gray"
        )))
        msg.put(this.formatRank())
        msg.put(JSONObject(mapOf(
                "text" to " > ",
                "color" to "gray"
        )))
        msg.put(this.formatName())
        msg.put(JSONObject(mapOf(
                "text" to " >> ",
                "color" to "gray"
        )))
        msg.put(JSONObject(mapOf(
                "text" to message
        )))
        return msg
    }
}