package me.voidcrafted.bungeeplugins

import me.lucko.luckperms.LuckPerms
import net.md_5.bungee.api.connection.ProxiedPlayer
import net.md_5.bungee.api.event.ChatEvent
import net.md_5.bungee.api.plugin.Listener
import net.md_5.bungee.chat.ComponentSerializer
import net.md_5.bungee.event.EventHandler

class ChatHandler(plugin: BungeeSidePlugins) : Listener {
    val plugin = plugin

    @EventHandler
    fun onPlayerMessage(e: ChatEvent) {
        if (e.isCommand) return
        e.isCancelled = true
        val api = LuckPerms.getApi()

        val player = e.sender as ProxiedPlayer
        val playerPerms = api.getUser(player.uniqueId) ?: return
        val player_group = api.getGroup(playerPerms.primaryGroup) ?: return
        val player_rank = player_group.friendlyName
        var playerWithData = PlayerDataStorage.getPlayer((e.sender as ProxiedPlayer).uniqueId) ?: return
        var player_discord = playerWithData.tag
        val player_name = player.name
        val message = e.message
        var comps = ComponentSerializer.parse(ChatFormatter(player_name, player_group, player_rank, player_discord).format(message, (e.sender as ProxiedPlayer).server.info.name).toString(0))
        plugin.proxy.players.forEach { it.sendMessage(*comps) }

    }

}