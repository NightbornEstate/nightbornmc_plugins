package me.voidcrafted.bungeeplugins

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import kotlinx.coroutines.experimental.launch
import me.lucko.luckperms.LuckPerms
import me.lucko.luckperms.api.Node
import me.lucko.luckperms.api.User
import net.md_5.bungee.api.chat.TextComponent
import net.md_5.bungee.api.event.PostLoginEvent
import net.md_5.bungee.event.EventHandler
import net.md_5.bungee.api.plugin.Listener
import net.md_5.bungee.chat.ComponentSerializer
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

class DiscordAuthenticator(plugin: BungeeSidePlugins): Listener {
    val plugin = plugin

    fun checkAuth(uuid: UUID): PlayerWithData? {
        var resp = khttp.get("http://sunset:1038/api/get_player/$uuid").jsonObject
        if(resp.getBoolean("error")) return null
        var player = resp.getJSONObject("player")
        return PlayerWithData(player.getString("discord_id"), player.getString("rank"), uuid, player.getString("tag"))
    }

    @EventHandler
    fun onPostLoginEvent(e: PostLoginEvent) {
        launch {
            var auth = checkAuth(e.player.uniqueId)
            if (auth == null) {
                var algorithm = Algorithm.HMAC256("test")
                var id = RandomWordProvider().getRandomWord().replace("\n", "").replace("\r", "").replace(" ", "")
                var token = JWT.create()
                        .withIssuer("me.voidcrafted.bungeeplugins.DiscordAuthenticator")
                        .withClaim("minecraft_uuid", e.player.uniqueId.toString())
                        .withClaim("nonce", Date().time)
                        .withClaim("id", id)
                        .sign(algorithm)
                var msg = JSONArray()
                msg.put("")
                msg.put(JSONObject(mapOf(
                        "text" to "Disconnected\n",
                        "color" to "red"
                )))
                msg.put(JSONObject(mapOf(
                        "text" to "You need to link your account first. ",
                        "color" to "red"
                )))
                msg.put(JSONObject(mapOf(
                        "text" to "Go to #bot-commands and type ",
                        "color" to "gray"
                )))
                msg.put(JSONObject(mapOf(
                        "text" to "!link $id",
                        "color" to "aqua"
                )))
                msg.put(JSONObject(mapOf(
                        "text" to " then join back again!",
                        "color" to "gray"
                )))
                var comps = ComponentSerializer.parse(msg.toString(0))
                e.player.disconnect(*comps)
                println(khttp.post("http://sunset:1038/api/register_code", json = JSONObject(mapOf(
                        "auth" to "test",
                        "token" to token
                ))).text)
                return@launch
            }
            val api = LuckPerms.getApi()
            var groups = arrayOf( // Staff roles
                    "mafia_don", "tech", "mcadmin", "capo", "evtorg"
            )
            var u: User = api.getUser(e.player.uniqueId) ?: return@launch
            fun removePerm(node: Node) {
                u.clearMatching { it == node }
            }
            u.permissions.forEach {
                // Remove all group nodes that are admin groups
                if (it.isGroupNode &&  // If it's a group and in the list above
                        (groups.map { "group.$it" }.contains(it.permission))
                ) removePerm(it) // Fucking nuke it
            }
            u.setPermission(
                    api // set the right group
                            .nodeFactory
                            .newBuilder("group.${auth.rank}")
                            .build()
            )
            // Save them back
            api.userManager.saveUser(u)
            PlayerDataStorage.players += auth
        }
    }
}