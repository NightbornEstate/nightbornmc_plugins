package me.voidcrafted.bungeeplugins
import net.md_5.bungee.api.plugin.Listener
import net.md_5.bungee.api.ChatColor
import net.md_5.bungee.api.chat.TextComponent
import net.md_5.bungee.event.EventHandler
import net.md_5.bungee.api.event.ServerSwitchEvent

class JoinEventListener( plugin: BungeeSidePlugins ): Listener {
    val plugin = plugin

    @EventHandler
    fun onServerChange(e: ServerSwitchEvent) {
        plugin.proxy.broadcast(TextComponent( ChatColor.LIGHT_PURPLE.toString() + e.player.displayName + " -> " + e.player.server.info.name))
    }
}