package me.voidcrafted.bungeeplugins

import java.util.*

object PlayerReplies {
    var recentReplies: MutableMap<UUID, UUID> = mutableMapOf()
    fun addReplies(u1: UUID, u2: UUID) {
        recentReplies[u1] = u2
        recentReplies[u2] = u1
    }
}