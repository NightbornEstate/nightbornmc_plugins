package me.voidcrafted.bungeeplugins

import java.util.*

class PlayerWithData(discord_id: String, rank: String, uuid: UUID, tag: String) {
    var discord_id = discord_id
    var rank = rank
    var uuid = uuid
    var tag = tag
}
