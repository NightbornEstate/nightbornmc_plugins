package me.voidcrafted.bungeeplugins

import java.sql.Time
import java.util.*
import kotlin.math.floor

class RandomWordProvider {
    val randomWords = khttp.get("https://raw.githubusercontent.com/dwyl/english-words/master/words_alpha.txt").text.split("\n")
    fun getRandomWord(): String {
        var random = Random()
        random.setSeed(Date().time)
        var num = random.nextDouble()
        var word = randomWords[floor(num * randomWords.size).toInt()]
        println("Word: $word")
        return word
    }
}