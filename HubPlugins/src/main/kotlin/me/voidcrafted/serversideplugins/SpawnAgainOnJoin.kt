package me.voidcrafted.serversideplugins

import org.bukkit.GameMode
import org.bukkit.Location
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.EntityDamageEvent
import org.bukkit.event.entity.PlayerDeathEvent
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.event.player.PlayerMoveEvent
import org.bukkit.event.player.PlayerRespawnEvent

class SpawnAgainOnJoin(val plugins: HubPlugins) : Listener {
    @EventHandler
    fun onPlayerJoin(e: PlayerJoinEvent) {
        e.player.teleport(Location(e.player.world, -17.58, 85.0, -86.5, 180F, 0F))
        e.player.gameMode = GameMode.ADVENTURE
        e.player.inventory.clear()
        e.player.updateInventory()
    }
    @EventHandler
    fun onPlayerRespawn(e: PlayerRespawnEvent) {
        e.respawnLocation = Location(plugins.server.getWorld("void"), -17.58, 85.0, -86.5, 180F, 0F)
    }
    @EventHandler
    fun onHealth(e: EntityDamageEvent ) {
        e.isCancelled = true
    }
    @EventHandler
    fun onMove(e: PlayerMoveEvent) {
        if (e.to.blockY < 20) e.player.health = 0.0
    }
}
